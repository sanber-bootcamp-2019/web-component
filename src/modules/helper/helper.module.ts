import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OjekButtonComponent } from './components/button/button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    OjekButtonComponent
  ],
  declarations: [
    OjekButtonComponent
  ],
  providers: [],
})
export class HelperModule { }
