import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HelperModule } from '../modules/helper';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HelperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
